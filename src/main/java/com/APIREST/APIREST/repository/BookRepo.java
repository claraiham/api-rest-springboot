package com.APIREST.APIREST.repository;

import org.hibernate.mapping.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.APIREST.APIREST.Entity.Book;

@Repository
public interface BookRepo extends JpaRepository<Book, Long> {
    @Query("select b from Book b where b.title like %?1")
    Iterable<Book> findByTitle(String title);
}

