package com.APIREST.APIREST.Entity; // indiquer le chemin du fichier

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity; // import de la structure entity pour indiquer à springboot que c'est une table à créer dans la bdd
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="BOOK")
public class Book {
    //id (Long, clé primaire auto incrémentée), 
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    //créer des catégories


    @ManyToOne
    @JoinColumn(name="category_id")
    //@OnDelete(action = OnDeleteAction.CASCADE)
    //@JsonIgnore
    private Category category;

    //title (String, non null,longueur maximale de 100 caractères), 
    @Column(name="TITLE", length=100, nullable=false)
    private String title;

    //description (String, peut être null, est un texte long),
    @Column(name="DESCRIPTION", columnDefinition="TEXT", nullable=true)
    private String description;

    //available (Boolean, défaut à true)
    @Column(name="AVAILABLE")
    private Boolean available = true;

    

    //constructeur
    public Book() {}
    public Book(Long id,String title,String description,Boolean available){
        this.id = id;
        this.title = title;
        this.description = description;
        this.available = available;
    }

    // getter et setter 
    public Long getId(){
        return this.id;
    }

    public String getTitle(){
        return this.title;
    }

    public String getDescription(){
        return this.description;
    }

    public Boolean getAvailable(){
        return this.available;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isAvailable() {
        return this.available;
    }

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    public void setAvailable(Boolean available) {
        this.available = available;
    }

}