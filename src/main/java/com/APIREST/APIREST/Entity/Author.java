package com.APIREST.APIREST.Entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="AUTHOR")
public class Author {
    
    public Author(){}
//Ajoute les entités Author avec un identifiant (Long) auto-généré, un nom (String) et un prénom (String) 
    
//id (Long, clé primaire auto incrémentée), 
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    //firstName
    @Column(name="FIRSTNAME", columnDefinition="TEXT", nullable=true)
    private String firstName;

    //lastName
    @Column(name="LASTNAME", columnDefinition="TEXT", nullable=true)
    private String lastName;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}



