package com.APIREST.APIREST.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.APIREST.APIREST.Entity.Category;
import com.APIREST.APIREST.repository.CategoryRepository;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    private Category category;

    public Iterable<Category> getAllBooks(){
        return categoryRepository.findAll();
    }

    public Optional<Category> getBookById(Long id) {
        return categoryRepository.findById(id);
    }

    public Category newCategory(Category category){
        Category savedCategory = categoryRepository.save(category);
        return savedCategory;
    }

    public Category updateBook(Category category, Long id){
        Category depDB
            = categoryRepository.findById(id)
                  .get();
 
        if (Objects.nonNull(category.getTitle())
            && !"".equalsIgnoreCase(
                category.getTitle())) {
            depDB.setTitle(
                category.getTitle());
        }
 
        return categoryRepository.save(depDB);
    }

    // Optional<Book> bookData = bookRepo.findById(id);

    // if (bookData.isPresent()) {
    //     Book _book = bookData.get();
    //     System.out.println(_book);
    //     _book.setTitle(book.getTitle());
    //     _book.setDescription(book.getDescription());
    //     _book.setAvailable(book.isAvailable());
    //   return new ResponseEntity<>(bookRepo.save(_book), HttpStatus.OK);
    // } else {
    //     System.out.println("CA MARCHE PAS");
    //   return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    // }


    public ResponseEntity<Category> deleteCategory(Long id){
        try{
            categoryRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }

}
