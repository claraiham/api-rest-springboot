package com.APIREST.APIREST.Service;

import java.util.Objects;
import java.util.Optional;

import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.APIREST.APIREST.Entity.Book;
import com.APIREST.APIREST.repository.BookRepo;

@Service
public class BookService {
    @Autowired
    private BookRepo bookRepo;
    private Book book;

    public Iterable<Book> getAllBooks(){
        return bookRepo.findAll();
    }

    public Optional<Book> getBookById(Long id) {
        return bookRepo.findById(id);
    }

    public Book newBook(Book book){
        Book savedBook = bookRepo.save(book);
        return savedBook;
    }

    public Book updateBook(Book book, Long id){
        Book depDB
            = bookRepo.findById(id)
                  .get();
 
        if (Objects.nonNull(book.getTitle())
            && !"".equalsIgnoreCase(
                book.getTitle())) {
            depDB.setTitle(
                book.getTitle());
        }
 
        if (Objects.nonNull(
                book.getDescription())
            && !"".equalsIgnoreCase(
                book.getDescription())) {
            depDB.setDescription(
                book.getDescription());
        }

        if (Objects.nonNull(book.getAvailable())) {
                depDB.setAvailable(book.getAvailable());
        }
 
        return bookRepo.save(depDB);
    }

    // Optional<Book> bookData = bookRepo.findById(id);

    // if (bookData.isPresent()) {
    //     Book _book = bookData.get();
    //     System.out.println(_book);
    //     _book.setTitle(book.getTitle());
    //     _book.setDescription(book.getDescription());
    //     _book.setAvailable(book.isAvailable());
    //   return new ResponseEntity<>(bookRepo.save(_book), HttpStatus.OK);
    // } else {
    //     System.out.println("CA MARCHE PAS");
    //   return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    // }


    public ResponseEntity<Book> deleteBook(Long id){
        try{
            bookRepo.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }

    public Iterable<Book> getBookByTitle(String title){
        return bookRepo.findByTitle(title);
    }

}
