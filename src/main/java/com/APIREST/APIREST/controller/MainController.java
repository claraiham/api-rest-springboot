package com.APIREST.APIREST.controller;

import java.util.Optional;

import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.APIREST.APIREST.Entity.Book;
import com.APIREST.APIREST.Service.BookService;
import com.APIREST.APIREST.repository.BookRepo;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;




@Controller
@RestController
public class MainController {
    @Autowired
    private BookService bookService;
    private BookRepo bookRepo;

    //GET /books, 
    @GetMapping("books")
    @ResponseBody
    public Iterable<Book> getBooks() {
        return bookService.getAllBooks();
    }
    
    //GET /books/{id},
    @GetMapping("books/{id}")
    public Optional<Book> getBook(@PathVariable Long id) { 
        return bookService.getBookById(id);
    }
    
    //POST /books,
    @PostMapping("books")
    public Book createBooks(@RequestBody Book book) {
        return bookService.newBook(book);
    }
     
    //PUT /books/{id}
    @PutMapping("books/{id}")
    public Book putMethodName(@PathVariable Long id, @RequestBody Book book) {
        return bookService.updateBook(book, id);
    }
    

    //DELETE /books/{id}
    @DeleteMapping("books/{id}")
    public ResponseEntity<Book> deleteBook(@PathVariable Long id, Book book){
        return bookService.deleteBook(id);
    }
    
    //Crée l'endpoint /books/title/{title} permettant de trouver un livre par son titre. 
    //Pour se faire, il te faudra créer une "query methods" dans BookRepository
    @GetMapping("books/title/{title}")
    public Iterable<Book> getBookByTitle(@PathVariable String title) {
        return bookService.getBookByTitle(title);
    }
    
}
